package org.incava.java;


/**
 * A Javadoc tag.
 */
public class JavadocTag extends JavadocElement
{
    public JavadocTag(String text, Location start, Location end)
    {
        super(text, start, end);
    }

}
