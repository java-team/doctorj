package org.incava.java;


/**
 * The description section of a Javadoc comment.
 */
public class JavadocDescriptionNode extends JavadocElement
{
    public String description;

    public JavadocDescriptionNode(String description, Location start, Location end)
    {
        super(description, start, end);
        this.description = description;
    }

}
