Summary:   Text search application
Name:      doctorj
Version:   5.0.0
Release:   1
Epoch:     0
License:   LGPL
Group:     Development/Tools
URL:       http://doctorj.sourceforge.net/
Source:    http://prdownloads.sourceforge.net/doctorj/doctorj-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Packager:  Jeff Pace (jpace@incava.org)
Vendor:    incava.org

%description
DoctorJ analyzes Java code and Javadoc comments.

%prep
%setup -q

%build
make %{?_smp_mflags}

%install
[ "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf "$RPM_BUILD_ROOT"
%makeinstall

%clean
[ "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf "$RPM_BUILD_ROOT"

%files
%defattr(0644,root,root,0755)
%doc AUTHORS ChangeLog COPYING INSTALL NEWS README
%attr(0755,root,root) %{_bindir}/*
%{_mandir}/*/*
%attr(0755,root,root) %{_datadir}/doctorj/doctorj.jar
%{_datadir}/doctorj/words.*

%changelog
* Mon Apr 26 2004 Jeff Pace <jpace@incava.org> 5.0.0-1
- Java rewrite.
