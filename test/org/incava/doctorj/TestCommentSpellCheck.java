package org.incava.doctorj;

import java.io.*;
import java.util.*;
import junit.framework.TestCase;
import org.incava.log.Log;


public class TestCommentSpellCheck extends TestCase
{
    static class TestableCommentSpellCheck extends CommentSpellCheck
    {
        class Misspelling
        {
            public String word;

            public int position;

            public List nearMatches;

            public Misspelling(String word, int position, List nearMatches)
            {
                this.word = word;
                this.position = position;
                this.nearMatches = nearMatches;
            }

            public String toString()
            {
                return "[" + word + ", " + position + ", {" + nearMatches + "}";
            }
        }

        private List misspellings = new ArrayList();

        public void check(String desc)
        {
            misspellings = new ArrayList();
            super.check(desc);
        }
            
        protected void wordMisspelled(String word, int position, List nearMatches)
        {
            misspellings.add(new Misspelling(word, position, nearMatches));
        }
    }

    private static TestableCommentSpellCheck tcsc = new TestableCommentSpellCheck();
    static {
        tcsc.addDictionary("/usr/share/dict/words");
    }
    
    public TestCommentSpellCheck(String name)
    {
        super(name);
    }

    public void testOK()
    {
        String comment = "// This is a comment.";
        tcsc.check(comment);
        Log.log("misspellings: " + tcsc.misspellings);
        assertEquals(0, tcsc.misspellings.size());
    }
    
    public void testMisspelling()
    {
        String comment = "// This is comment has a mispelled word.";
        tcsc.check(comment);
        Log.log("misspellings: " + tcsc.misspellings);
        assertEquals(1, tcsc.misspellings.size());
    }

    public void testMisspellings()
    {
        String comment = "// This is comment has twoo mispelled word.";
        tcsc.check(comment);
        Log.log("misspellings: " + tcsc.misspellings);
        assertEquals(2, tcsc.misspellings.size());
    }

    public void testOKCapitalized()
    {
        String comment = "// This is a Comment.";
        tcsc.check(comment);
        Log.log("misspellings: " + tcsc.misspellings);
        assertEquals(0, tcsc.misspellings.size());
    }

    public void testOKPreBlock()
    {
        {
            String comment = ("/** This is a comment.\n" +
                              "  *\n" +
                              "  *<pre>\n" +
                              "  * nuffim eh?\n" +
                              "  *</pre>\n" +
                              "  */\n");
            tcsc.check(comment);
            Log.log("misspellings: " + tcsc.misspellings);
            assertEquals(0, tcsc.misspellings.size());
        }

        {
            String comment = ("/** This is a comment.\n" +
                              "  *\n" +
                              "  *<pre>\n" +
                              "  * nuttin in heer shuld bie checkt\n" +
                              "  *</pre>\n" +
                              "  */\n");
            tcsc.check(comment);
            Log.log("misspellings: " + tcsc.misspellings);
            assertEquals(0, tcsc.misspellings.size());
        }

        {
            String comment = ("/** This is a comment.\n" +
                              "  *\n" +
                              "  *<pre>\n" +
                              "  * This pre block has no end.\n" +
                              "  */\n");
            tcsc.check(comment);
            Log.log("misspellings: " + tcsc.misspellings);
            assertEquals(0, tcsc.misspellings.size());
        }
    }

    public void testOKCodeBlock()
    {
        {
            String comment = ("/** This is a comment that refers to <code>str</code>. */");
            tcsc.check(comment);
            Log.log("misspellings: " + tcsc.misspellings);
            assertEquals(0, tcsc.misspellings.size());
        }

        {
            String comment = ("/** This is a comment that refers to <code>somethingthatdoesnotend */");
            tcsc.check(comment);
            Log.log("misspellings: " + tcsc.misspellings);
            assertEquals(0, tcsc.misspellings.size());
        }
    }

    public void testOKLink()
    {
        {
            String comment = ("/** This is a comment that refers to a {@link tosomewhere}. */");
            tcsc.check(comment);
            Log.log("misspellings: " + tcsc.misspellings);
            assertEquals(0, tcsc.misspellings.size());
        }

        {
            String comment = ("/** This is a comment that refers to a {@link tosomewherefarbeyond */");     // "}" -- for Emacs
            tcsc.check(comment);
            Log.log("misspellings: " + tcsc.misspellings);
            assertEquals(0, tcsc.misspellings.size());
        }
    }
    
}
