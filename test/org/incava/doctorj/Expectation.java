package org.incava.doctorj;

import java.util.*;
import junit.framework.TestCase;
import org.incava.analysis.Violation;


public class Expectation
{
    public String message;

    public int level;

    public int beginLine;
    
    public int beginColumn;

    public int endLine;
    
    public int endColumn;
    
    public Expectation(int level, String message, int beginLine, int beginColumn, int endLine, int endColumn)
    {
        this.level = level;
        this.message = message;
        this.beginLine = beginLine;
        this.beginColumn = beginColumn;
        this.endLine = endLine;
        this.endColumn = endColumn;
    }

    public String toString() 
    {
        return "{lvl: " + level + ", msg: " + message + ", bgLn: " + beginLine + ", bgCol: " + beginColumn + ", endLn: " + endLine + ", endCol: " + endColumn + "}";
    }
}
