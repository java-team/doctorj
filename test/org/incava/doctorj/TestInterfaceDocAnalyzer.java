package org.incava.doctorj;

import junit.framework.TestCase;


public class TestInterfaceDocAnalyzer extends TestTypeDocAnalyzer
{
    public TestInterfaceDocAnalyzer(String name)
    {
        super(name);
    }

    public void testInterfaceAuthorWithText()
    {
        evaluate("/** This is a description.\n" +
                 "  * @author e. e. cummings\n" +
                 "  */\n" +
                 "interface TestInterfaceAuthorTag {\n" +
                 "}\n",
                 new Object[][] { 
                 });

        evaluate("/** This is a description.\n" +
                 "  * @author I\n" +
                 "  */\n" +
                 "interface TestInterfaceAuthorTag {\n" +
                 "}\n",
                 new Object[][] { 
                 });

        evaluate("/** This is a description.\n" +
                 "  * @author fred\n" +
                 "  */\n" +
                 "interface TestInterfaceAuthorTag {\n" +
                 "}\n",
                 new Object[][] { 
                 });
    }

    public void testInterfaceAuthorWithoutText()
    {
        evaluate("/** This is a description.\n" +
                 "  * @author\n" +
                 "  */\n" +
                 "interface TestInterfaceAuthorTag {\n" +
                 "}\n",
                 new Object[][] { 
                     { TypeDocAnalyzer.MSG_AUTHOR_WITHOUT_NAME, new Integer(2), new Integer(5), new Integer(2), new Integer(11) }
                 });
    }
    
    public void testInterfaceAuthorWithoutTextSpaces()
    {
        evaluate("/** This is a description.\n" +
                 "  * @author   \n" +
                 "  */\n" +
                 "interface TestInterfaceAuthorTag {\n" +
                 "}\n",
                 new Object[][] { 
                     { TypeDocAnalyzer.MSG_AUTHOR_WITHOUT_NAME, new Integer(2), new Integer(5), new Integer(2), new Integer(11) }
                 });

        evaluate("/** This is a description.\n" +
                 "  * @author \n" +
                 "  */\n" +
                 "interface TestInterfaceAuthorTag {\n" +
                 "}\n",
                 new Object[][] { 
                     { TypeDocAnalyzer.MSG_AUTHOR_WITHOUT_NAME, new Integer(2), new Integer(5), new Integer(2), new Integer(11) }
                 });
    }
    

    public void testInterfaceVersionWithText()
    {
        evaluate("/** This is a description.\n" +
                 "  * @version 1.1.2\n" +
                 "  */\n" +
                 "interface TestInterfaceVersionTag {\n" +
                 "}\n",
                 new Object[][] { 
                 });

        evaluate("/** This is a description.\n" +
                 "  * @version 1\n" +
                 "  */\n" +
                 "interface TestInterfaceVersionTag {\n" +
                 "}\n",
                 new Object[][] { 
                 });
    }

    public void testInterfaceVersionWithoutText()
    {
        evaluate("/** This is a description.\n" +
                 "  * @version\n" +
                 "  */\n" +
                 "interface TestInterfaceVersionTag {\n" +
                 "}\n",
                 new Object[][] { 
                     { TypeDocAnalyzer.MSG_VERSION_WITHOUT_TEXT, new Integer(2), new Integer(5), new Integer(2), new Integer(12) }
                 });
    }
    
    public void testInterfaceVersionWithoutTextSpaces()
    {
        evaluate("/** This is a description.\n" +
                 "  * @version   \n" +
                 "  */\n" +
                 "interface TestInterfaceVersionTag {\n" +
                 "}\n",
                 new Object[][] { 
                     { TypeDocAnalyzer.MSG_VERSION_WITHOUT_TEXT, new Integer(2), new Integer(5), new Integer(2), new Integer(12) }
                 });

        evaluate("/** This is a description.\n" +
                 "  * @version \n" +
                 "  */\n" +
                 "interface TestInterfaceVersionTag {\n" +
                 "}\n",
                 new Object[][] { 
                     { TypeDocAnalyzer.MSG_VERSION_WITHOUT_TEXT, new Integer(2), new Integer(5), new Integer(2), new Integer(12) }
                 });
    }


    public void testInterfaceSerialWithText()
    {
        evaluate("/** This is a description.\n" +
                 "  * @serial This describes the serial field.\n" +
                 "  */\n" +
                 "interface TestInterfaceSerialTag {\n" +
                 "}\n",
                 new Object[][] { 
                 });

        evaluate("/** This is a description.\n" +
                 "  * @serial description\n" +
                 "  */\n" +
                 "interface TestInterfaceSerialTag {\n" +
                 "}\n",
                 new Object[][] { 
                 });
    }

    public void testInterfaceSerialWithoutText()
    {
        evaluate("/** This is a description.\n" +
                 "  * @serial\n" +
                 "  */\n" +
                 "interface TestInterfaceSerialTag {\n" +
                 "}\n",
                 new Object[][] { 
                     { TypeDocAnalyzer.MSG_SERIAL_WITHOUT_TEXT, new Integer(2), new Integer(5), new Integer(2), new Integer(11) }
                 });
    }
    
    public void testInterfaceSerialWithoutTextSpaces()
    {
        evaluate("/** This is a description.\n" +
                 "  * @serial   \n" +
                 "  */\n" +
                 "interface TestInterfaceSerialTag {\n" +
                 "}\n",
                 new Object[][] { 
                     { TypeDocAnalyzer.MSG_SERIAL_WITHOUT_TEXT, new Integer(2), new Integer(5), new Integer(2), new Integer(11) }
                 });

        evaluate("/** This is a description.\n" +
                 "  * @serial \n" +
                 "  */\n" +
                 "interface TestInterfaceSerialTag {\n" +
                 "}\n",
                 new Object[][] { 
                     { TypeDocAnalyzer.MSG_SERIAL_WITHOUT_TEXT, new Integer(2), new Integer(5), new Integer(2), new Integer(11) }
                 });
    }

}
