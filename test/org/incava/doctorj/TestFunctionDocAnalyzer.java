package org.incava.doctorj;

import junit.framework.TestCase;


public class TestFunctionDocAnalyzer extends Tester
{
    public TestFunctionDocAnalyzer(String name)
    {
        super(name);
    }

    public void testMethodSerialDataOK()
    {
        evaluate("/** This is a description. */\n" +
                 "class Test {\n" +
                 "    /** This is a description.\n" +
                 "      * @serialData Something.\n" +
                 "      */\n" +
                 "    void f() {}\n" +
                 "}\n",
                 new Object[][] { 
                 });
    }

    public void testCtorSerialDataOK()
    {
        evaluate("/** This is a description. */\n" +
                 "class Test {\n" +
                 "    /** This is a description.\n" +
                 "      * @serialData Something.\n" +
                 "      */\n" +
                 "    Test() {}\n" +
                 "}\n",
                 new Object[][] { 
                 });
    }

    public void testMethodSerialDataUndescribed()
    {
        evaluate("/** This is a description. */\n" +
                 "class Test {\n" +
                 "    /** This is a description.\n" +
                 "      * @serialData \n" +
                 "      */\n" +
                 "    void f() {}\n" +
                 "}\n",
                 new Object[][] { 
                     { FunctionDocAnalyzer.MSG_SERIALDATA_WITHOUT_DESCRIPTION, new Integer(4), new Integer(9), new Integer(4), new Integer(19) }
                 });
    }

    public void testCtorSerialDataUndescribed()
    {
        evaluate("/** This is a description. */\n" +
                 "class Test {\n" +
                 "    /** This is a description.\n" +
                 "      * @serialData \n" +
                 "      */\n" +
                 "    Test() {}\n" +
                 "}\n",
                 new Object[][] { 
                     { FunctionDocAnalyzer.MSG_SERIALDATA_WITHOUT_DESCRIPTION, new Integer(4), new Integer(9), new Integer(4), new Integer(19) }
                 });
    }
    
}
