package org.incava.doctorj;

import junit.framework.TestCase;


public class TestFieldDocAnalyzer extends Tester
{
    public TestFieldDocAnalyzer(String name)
    {
        super(name);
    } 

    public void testSerialFieldOK()
    {
        evaluate("/** This is a description. */\n" +
                 "class Test {\n" +
                 "    /** This is a description.\n" +
                 "      * @serialField one int What one is." +
                 "      */\n" +
                 "    ObjectStreamField[] serialPersistentFields = { \n" +
                 "        new ObjectStreamField(\"one\",  Integer.TYPE) \n" +
                 "    };\n" +
                 "}\n",
                 new Object[][] { 
                 });
    }

    public void testSerialFieldNoDoc()
    {
        evaluate("/** This is a description. */\n" +
                 "class Test {\n" +
                 "    /** This is a description.\n" +
                 "      * @serialField" +
                 "      */\n" +
                 "    ObjectStreamField[] serialPersistentFields = { \n" +
                 "        new ObjectStreamField(\"one\",  Integer.TYPE) \n" +
                 "    };\n" +
                 "}\n",
                 new Object[][] { 
                     { FieldDocAnalyzer.MSG_SERIALFIELD_WITHOUT_NAME_TYPE_AND_DESCRIPTION, new Integer(4), new Integer(9), new Integer(4), new Integer(20) }
                 });

        evaluate("/** This is a description. */\n" +
                 "class Test {\n" +
                 "    /** This is a description.\n" +
                 "      * @serialField  " +
                 "      */\n" +
                 "    ObjectStreamField[] serialPersistentFields = { \n" +
                 "        new ObjectStreamField(\"one\",  Integer.TYPE) \n" +
                 "    };\n" +
                 "}\n",
                 new Object[][] { 
                     { FieldDocAnalyzer.MSG_SERIALFIELD_WITHOUT_NAME_TYPE_AND_DESCRIPTION, new Integer(4), new Integer(9), new Integer(4), new Integer(20) }
                 });
    }

    public void testSerialFieldNoDescription()
    {
        evaluate("/** This is a description. */\n" +
                 "class Test {\n" +
                 "    /** This is a description.\n" +
                 "      * @serialField one int " +
                 "      */\n" +
                 "    ObjectStreamField[] serialPersistentFields = { \n" +
                 "        new ObjectStreamField(\"one\",  Integer.TYPE) \n" +
                 "    };\n" +
                 "}\n",
                 new Object[][] { 
                     { FieldDocAnalyzer.MSG_SERIALFIELD_WITHOUT_DESCRIPTION, new Integer(4), new Integer(22), new Integer(4), new Integer(29) }
                 });
    }

    public void testSerialFieldNoTypeNorDescription()
    {
        evaluate("/** This is a description. */\n" +
                 "class Test {\n" +
                 "    /** This is a description.\n" +
                 "      * @serialField one " +
                 "      */\n" +
                 "    ObjectStreamField[] serialPersistentFields = { \n" +
                 "        new ObjectStreamField(\"one\",  Integer.TYPE) \n" +
                 "    };\n" +
                 "}\n",
                 new Object[][] { 
                     { FieldDocAnalyzer.MSG_SERIALFIELD_WITHOUT_TYPE_AND_DESCRIPTION, new Integer(4), new Integer(22), new Integer(4), new Integer(25) }
                 });
    }
    
}
